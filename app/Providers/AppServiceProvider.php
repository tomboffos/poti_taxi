<?php

namespace App\Providers;

use App\Models\Order;
use App\Models\OrderDriver;
use App\Observers\OrderDriverObserver;
use App\Observers\OrderObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Order::observe(OrderObserver::class);
        OrderDriver::observe(OrderDriverObserver::class);
    }
}
