<?php


namespace App\Services\Main;


use App\Models\Device;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class PushNotification
{

    public function insertToken($token, User $user)
    {
        if (!$user->checkDeviceExists($token))
            Device::create([
                'token' => $token,
                'user_id' => $user->id
            ]);
    }

    public function deleteToken($token, User $user)
    {
        if ($user->checkDeviceExists($token))
            Device::where('token', $token)->first()->delete();
    }

    public static function send($device_tokens, string $title, string $text)
    {
        $response = Http::withHeaders([
            'Authorization' => "key=" . env('PUSH_NOTIFICATION'),
            'Content-Type' => 'application/json',
        ])->post('https://fcm.googleapis.com/fcm/send', [
            "registration_ids" => $device_tokens,
            'notification' => [
                'title' => $title,
                'body' => $text,
                "sound" => 'notification.wav',
            ],
        ]);
//        Log::error('logging '.$response->);

        return $response->successful();
    }


}
