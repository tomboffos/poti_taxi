<?php


namespace App\Services\User\Interactions;


use App\Models\Area;
use App\Models\Dish;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Log;

class OrderService
{
    const ORDER_STATUS = 1;
    const ORDER_STATUS_RESTAURANT = 6;

    public function setInitialData($validatedData, $user)
    {
        $validatedData['user_id'] = $user->id;
        $validatedData['order_status_id'] = self::ORDER_STATUS;
        $validatedData['area_id'] = $this->findInArray($validatedData['point'])->id;
        $validatedData['order_type_id'] = 1;

        return $validatedData;
    }

    public function setInitialDataForRestaurants($validatedData, $user)
    {
        return array_merge($validatedData, [
            'user_id' => $user->id,
            'order_status_id' => self::ORDER_STATUS_RESTAURANT,
            'area_id' => $this->findInArray($validatedData['point'])->id,
            'order_type_id' => 2,
            'price' => $this->calculateTotalPrice($validatedData['basket'])
        ]);
    }

    public function collectOrderDetails($basket, Order $order)
    {
        foreach (json_decode($basket, true) as $item)
            OrderDetail::create([
                'dish_id' => $item['id'],
                'quantity' => $item['quantity'],
                'order_id' => $order->id,
                'price' => Dish::find($item['id'])->price * $item['quantity']
            ]);
    }

    public function calculateTotalPrice($basket)
    {
        $price = 0;
        foreach (json_decode($basket, true) as $item)
            $price += Dish::find($item['id'])->price * $item['quantity'];
        return $price;
    }

    public function findInArray($point)
    {
        $areas = Area::get();
        $point = json_decode($point);

        $possibleArea = null;
        Log::error('message ' . $point[0]);

        foreach ($areas as $area) {
            if ($this->checkInArrays($area->area, $point)) {
                $possibleArea = $area;
                break;
            }
        }

        return $possibleArea;

    }

    public function checkInArrays($area, $point)
    {
        //point[0] = latitude
        //point[1] = longitude
        $points_polygon = count($area) - 1;
        $vertices_x = $this->getOnlyLongitudes($area, 0);
        $vertices_y = $this->getOnlyLongitudes($area, 1);
        $i = $j = $c = 0;

        for ($i = 0, $j = $points_polygon; $i < $points_polygon;
             $j = $i++) {
            if ((($vertices_y[$i] > $point[1] != ($vertices_y[$j] > $point[1])) &&
                ($point[0] < ($vertices_x[$j] - $vertices_x[$i]) * ($point[1] - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i])))
                $c = !$c;
        }

        return $c;

    }

    public function getOnlyLongitudes($area, $index = 0)
    {
        $points_x = [];
        foreach ($area as $item)
            $points_x[] = $item[$index];
        return $points_x;
    }

    public function distanceBetweenTwoPoints($origin, $destination, $earthRadius = 6371000)
    {
        $latFrom = deg2rad($origin[0]);
        $lonFrom = deg2rad($origin[1]);
        $latTo = deg2rad($destination[0]);
        $lonTo = deg2rad($destination[1]);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }

}
