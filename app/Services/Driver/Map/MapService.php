<?php


namespace App\Services\Driver\Map;


use App\Models\OrderDriver;

class MapService
{
    public function distanceBetweenPoints($initialCoordinates, $secondCoordinates)
    {
        $theta = $initialCoordinates[1] - $secondCoordinates[1];
        $distance = sin(deg2rad($initialCoordinates[0])) - sin(deg2rad($secondCoordinates[0]))
            + cos(deg2rad($secondCoordinates[0])) * cos(deg2rad($secondCoordinates[0])) * cos(deg2rad($theta));

        $distance = acos($distance);
        $distance = rad2deg($distance) * 60 * 1.1515 * 1.609344;

        return round($distance, 2);
    }

    public function sendToOrders($orderId,$orderDriver)
    {
        foreach (OrderDriver::where('order_id', $orderId)->where('sent', 0)->orderBy('distance', 'asc')->get() as $item) {
            if ((OrderDriver::where('order_id', $orderId)->where('sent', 1)->orderBy('distance', 'desc')->first() != null
                    && OrderDriver::where('order_id', $orderId)->where('sent', 1)->orderBy('distance', 'desc')->first()->distance > $item->distance) ||
                OrderDriver::where('order_id', $orderId)->where('sent', 1)->count() == 0)
                $orderDriver->update([
                    'sent' => 1
                ]);

        }
    }
}
