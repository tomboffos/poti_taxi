<?php


namespace App\Services\Driver\Auth;


class ImageService
{

    public static function storeImages($images)
    {
        $imagesArray = [];

        foreach ($images as $image){
            $imagesArray[] = is_file($image) ? $image->store('driver_profiles') : $image;
        }

        return json_encode($imagesArray);
    }

    public static function showImages($images)
    {
        $imagesArray = [];
        foreach ($images as $image)
            $imagesArray[] = asset($image);
        return $imagesArray;
    }
}
