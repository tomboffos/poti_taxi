<?php

namespace App\Http\Controllers\Api\Driver\Main;

use App\Http\Controllers\Controller;
use App\Http\Resources\Driver\Info\DriverUserResource;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    //
    public function index(Request $request)
    {
        return new DriverUserResource($request->user());
    }
}
