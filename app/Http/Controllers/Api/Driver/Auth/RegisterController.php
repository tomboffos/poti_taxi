<?php

namespace App\Http\Controllers\Api\Driver\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Driver\Auth\RegisterRequest;
use App\Http\Resources\Driver\Info\DriverUserResource;
use App\Models\Driver;
use App\Models\User;
use App\Services\Main\PushNotification;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    //
    private $push;
    const DRIVER_ROLE_ID = 2;

    public function __construct()
    {
        $this->push = new PushNotification();
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->validated();

        $user = User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'phone' => $data['phone'],
            'password' => $data['password'],
            'role_id' => self::DRIVER_ROLE_ID,
            'email' => $data['email']
        ]);

        $driver = Driver::create([
            'iin' => $data['iin'],
            'car_model' => $data['car_model'],
            'car_color' => $data['car_color'],
            'car_id' => $data['car_id'],
            'avatar' => $data['avatar'],
            'driver_image' => $data['driver_image'],
            'user_id' => $user->id,
        ]);

        $this->push->insertToken($data['token'], $user);

        return response([
            'user' => new DriverUserResource($user),
            'token' => $user->createToken(env('APP_NAME'))->plainTextToken,
            'driver' => $driver
        ], 201);
    }
}
