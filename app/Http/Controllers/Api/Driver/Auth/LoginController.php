<?php

namespace App\Http\Controllers\Api\Driver\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Driver\Auth\LoginRequest;
use App\Http\Resources\Driver\Info\DriverUserResource;
use App\Models\User;
use App\Services\Main\PushNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    //

    private $push;

    public function __construct()
    {
        $this->push = new PushNotification();
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();


        if ($user && Hash::check($request->password, $user['password'])) {
            $this->push->insertToken($request->token, $user);
            return response([
                'user' => new DriverUserResource($user),
                'token' => $user->createToken(env('APP_NAME'))->plainTextToken
            ], 200);
        }

        return response([
            'message' => 'Не правильный логин или пароль'
        ], 400);
    }
}
