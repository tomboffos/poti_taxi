<?php

namespace App\Http\Controllers\Api\Driver\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Interactions\OrderRequest;
use App\Http\Resources\User\Interactions\OrderResource;
use App\Models\Order;
use App\Services\User\Interactions\OrderService;
use Illuminate\Http\Request;

class DriverOrderController extends Controller
{
    //
    /**
     * @var OrderService
     */
    private $service;

    public function __construct()
    {
        $this->service = new OrderService();
    }

    public function index(Request $request)
    {
        return OrderResource::collection($request->user()->driver->orders);
    }

    public function update(Order $order, Request $request)
    {

        $order->update([
            'order_status_id' => $request->order_status_id,
            'cancel_reason_id' => $request->has('cancel_reason_id') ? $request->cancel_reason_id : null,
            'cancel_comment' => $request->has('cancel_comment') ? $request->cancel_comment : null,
            'driver_id' => $request->has('accept') ? $request->user()->driver->id : null,
            'start_point' => $request->has('start_point') ? $request->start_point : null,
            'end_point' => $request->has('end_point') ? $request->end_point : null,
            'distance' => $request->has('end_point') ? $this->service->distanceBetweenTwoPoints($order->point, $request->end_point) : null,
            'price' => $request->has('end_point') && $this->service->distanceBetweenTwoPoints($order->point, $request->end_point) > $order->area->min_free_distance ?
                (($this->service->distanceBetweenTwoPoints($order->point, $request->end_point) - $order->area->min_free_distance)
                    * $order->price_per_kilometer) * $order->area->price : null
        ]);

        return new OrderResource($order);
    }
}
