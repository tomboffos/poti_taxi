<?php

namespace App\Http\Controllers\Api\Driver\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Driver\Interactions\OrderDriverStoreRequest;
use App\Models\Order;
use App\Models\OrderDriver;
use App\Services\Driver\Map\MapService;
use App\Services\User\Interactions\OrderService;
use Illuminate\Http\Request;

class OrderDriverController extends Controller
{
    //
    private $map;
    /**
     * @var OrderService
     */
    private $service;

    public function __construct()
    {
        $this->map = new MapService();
        $this->service = new OrderService();
    }

    public function store(OrderDriverStoreRequest $request)
    {
        $order = Order::find($request->order_id);

        $orderDriver = OrderDriver::create([
            'distance' => $this->map->distanceBetweenPoints(json_decode($order->point), json_decode($request->location)),
            'sent' => 0,
            'location' => $request->location,
            'user_id' => $request->user()->id,
            'order_id' => $order->id
        ]);

        if ($order->order_status_id == 1 && $this->service->checkInArrays($order->area->area, json_decode($orderDriver->location)))
            $this->map->sendToOrders($order->id, $orderDriver);

        return response([

        ], 200);
    }
}
