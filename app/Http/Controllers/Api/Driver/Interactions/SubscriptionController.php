<?php

namespace App\Http\Controllers\Api\Driver\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Resources\Driver\Interactions\SubscriptionResource;
use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    //
    public function index()
    {
        return SubscriptionResource::collection(Subscription::get());
    }


}
