<?php

namespace App\Http\Controllers\Api\Driver\Interactions;

use App\Http\Controllers\Controller;
use App\Models\CancelReason;
use Illuminate\Http\Request;

class CancelReasonController extends Controller
{
    //
    public function index(Request $request)
    {
        return CancelReason::get();
    }
}
