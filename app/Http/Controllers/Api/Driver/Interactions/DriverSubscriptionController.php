<?php

namespace App\Http\Controllers\Api\Driver\Interactions;

use App\Http\Controllers\Controller;
use App\Models\DriverSubscription;
use App\Services\Main\Paybox;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DriverSubscriptionController extends Controller
{
    //
    private $payment;

    public function __construct()
    {
        $this->payment = new Paybox();
    }

    public function store(Request $request)
    {
        $driverSubscription = DriverSubscription::create([
            'driver_id' => $request->user()->driver->id,
            'subscription_id' => $request->subscription_id,
            'payment_status_id' => 1
        ]);


        $this->payment->setQuery([
            'pg_amount' => $driverSubscription->subscription->price,
            'pg_order_id' => (string)$driverSubscription->id,
            'pg_user_id' => (string)$request->user()->id,
            'pg_salt' => Str::random(10),
            'pg_result_url' => route('subscription.result'),
            'pg_success_url' => route('subscription.success'),
            'pg_fail_url' => route('subscription.fail'),
            'pg_description' => 'Оплата тарифа '.$driverSubscription->id
        ]);

        return response([
            'payment' => $this->payment->paymentLink()
        ],200);


    }

    public function result(Request $request)
    {
        if ($request->pg_result) {
            $subscription = DriverSubscription::find($request->pg_order_id);
            $subscription->update([
                'payment_status_id' => 2,
                'payed' => \DateTime::now(),
            ]);
            $subscription->driver->update([
                'blocked' => false
            ]);
        }
    }
}
