<?php

namespace App\Http\Controllers\Api\User\Restaurants;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\Restaurants\DishCategoryResource;
use App\Http\Resources\User\Restaurants\OrganizationResource;
use App\Models\Organization;
use App\Models\OrganizationCategory;
use App\Services\User\Interactions\OrderService;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    //
    private $service;

    public function __construct()
    {
        $this->service = new OrderService();
    }

    public function index(Request $request)
    {
        if (!$this->service->findInArray($request->point))
            return response([
                'message' => 'Пока что ваш район не обслуживается'
            ], 400);

        if ($request->has('organization_category'))
            return OrganizationResource::collection(OrganizationCategory::find($request->organization_category)->organizations
                ->where('area_id', $this->service->findInArray($request->point))->get());

        return OrganizationResource::collection(Organization::where('area_id', $this->service->findInArray($request->point)->id)->get());
    }

    public function show(Organization $organization)
    {
        return DishCategoryResource::collection($organization->categories);
    }

}
