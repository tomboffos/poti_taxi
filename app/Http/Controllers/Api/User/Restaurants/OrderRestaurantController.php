<?php

namespace App\Http\Controllers\Api\User\Restaurants;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Restaurants\OrderStoreRequest;
use App\Http\Resources\Api\User\Restaurants\OrderRestaurantResource;
use App\Http\Resources\User\Interactions\OrderResource;
use App\Models\Order;
use App\Services\User\Interactions\OrderService;
use Illuminate\Http\Request;

class OrderRestaurantController extends Controller
{
    //
    /**
     * @var OrderService
     */
    private $service;

    public function __construct()
    {
        $this->service = new OrderService();
    }

    public function index(Request $request)
    {
        return OrderResource::collection($request->user()->restaurantOrders);
    }

    public function store(OrderStoreRequest $request)
    {
        if (!$this->service->findInArray($request->point))
            return response([
                'message' => 'Пока что ваш район не обслуживается'
            ], 400);

        $order = Order::create($this->service->setInitialDataForRestaurants($request->validated(), $request->user()));

        $this->service->collectOrderDetails($request->basket, $order);

        return new OrderRestaurantResource($order);
    }
}
