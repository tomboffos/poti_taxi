<?php

namespace App\Http\Controllers\Api\User\Restaurants;

use App\Http\Controllers\Controller;
use App\Models\OrganizationCategory;
use Illuminate\Http\Request;

class OrganizationCategoryController extends Controller
{
    //
    public function index()
    {
        return OrganizationCategory::get();
    }
}
