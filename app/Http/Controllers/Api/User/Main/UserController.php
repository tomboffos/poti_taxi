<?php

namespace App\Http\Controllers\Api\User\Main;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\Info\UserResource;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function index(Request $request)
    {
        return new UserResource($request->user());
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response([],200);
    }
}
