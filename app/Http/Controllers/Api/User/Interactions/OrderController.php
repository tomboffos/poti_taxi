<?php

namespace App\Http\Controllers\Api\User\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Interactions\OrderRequest;
use App\Http\Resources\User\Interactions\OrderResource;
use App\Models\Order;
use App\Services\User\Interactions\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    private $service;

    public function __construct()
    {
        $this->service = new OrderService();
    }

    public function store(OrderRequest $request)
    {
        if (!$this->service->findInArray($request->point))
            return  response([
                'message' => 'Пока что ваш район не обслуживается'
            ],400);
        return new OrderResource(Order::create($this->service->setInitialData($request->validated(), $request->user())));
    }

    public function update(Request $request,Order $order)
    {
        $order->update([
            'order_status_id' => $request->order_status_id
        ]);

        return new OrderResource($order);
    }

    public function index(Request $request)
    {
        return OrderResource::collection($request->user()->orders);
    }
}
