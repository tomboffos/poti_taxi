<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Auth\RegisterRequest;
use App\Http\Resources\User\Info\UserResource;
use App\Models\User;
use App\Services\Main\PushNotification;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    //
    private $push;

    const USER_ROLE = 1;

    public function __construct()
    {
        $this->push = new PushNotification();
    }

    public function register(RegisterRequest $request)
    {

        $data = $request->except(['token']);

        $data['role_id'] = self::USER_ROLE;

        $user = User::create($data);

        $this->push->insertToken($request->token, $user);

        return response([
            'user' => new UserResource($user),
            'token' => $user->createToken(env('APP_NAME'))->plainTextToken
        ], 201);
    }
}
