<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Auth\ForgetRequest;
use App\Http\Requests\Api\User\Auth\LoginRequest;
use App\Http\Resources\User\Info\UserResource;
use App\Mail\ForgetPassword;
use App\Models\User;
use App\Services\Main\PushNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    //
    private $push;

    public function __construct()
    {
        $this->push = new PushNotification();
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();

        $this->push->insertToken($request->token,$user);

        if ($user && Hash::check($request->password, $user->password))
            return response([
                'user' => new UserResource($user),
                'token' => $user->createToken(env('APP_NAME'))->plainTextToken
            ], 200);
        return response([
            'message' => 'Не правильный номер телефона или пароль'
        ], 400);
    }

    public function forget(ForgetRequest $request)
    {
        $password = Str::random(10);

        $user = User::where('phone',$request->phone)->first();

        $user->update([
            'password' => $password
        ]);

        Mail::to($user)->send(new ForgetPassword($user,$password));
        return response([],200);
    }
}
