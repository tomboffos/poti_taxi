<?php

namespace App\Http\Resources\Api\User\Restaurants;

use App\Http\Resources\Driver\Info\UserDriverResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderRestaurantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'cancel_reason' => $this->cancel_reason,
            'driver' => new UserDriverResource($this->driver),
            'user' => $this->user,
            'point' => $this->point,
            'start_point' => $this->start_point,
            'created_at' => $this->created_at->format('d.m.y h-m'),
            'details' => OrderDetailResource::collection($this->details),
            'organization' => $this->organization
        ];
    }
}
