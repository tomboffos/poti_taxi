<?php

namespace App\Http\Resources\User\Info;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'surname' => $this->surname,
            'email' => $this->email,
            'role_id' => $this->role_id,
            'accepted' => $this->driver != null ? $this->driver->accepted : false,
            'blocked' => $this->driver != null ? $this->driver->blocked : true,
        ];
    }
}
