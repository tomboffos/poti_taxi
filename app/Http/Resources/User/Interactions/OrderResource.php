<?php

namespace App\Http\Resources\User\Interactions;

use App\Http\Resources\Driver\Info\DriverResource;
use App\Http\Resources\Driver\Info\UserDriverResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'area' => $this->area,
            'cancel_reason' => $this->cancelReason,
            'driver' => new UserDriverResource($this->driver),
            'user' => $this->user,
            'id' => $this->id,
            'point' => $this->point,
            'start_point' => $this->start_point,
            'created_at' => $this->created_at->format('d.m.y h-m')
        ];
    }
}
