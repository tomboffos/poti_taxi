<?php

namespace App\Http\Resources\User\Restaurants;

use Illuminate\Http\Resources\Json\JsonResource;

class OrganizationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => str_contains('https://',$this->image) ? $this->image : asset($this->image),
            'description' => $this->description,
            'short_description' => $this->short_description,
            'point' => $this->point
        ];
    }
}
