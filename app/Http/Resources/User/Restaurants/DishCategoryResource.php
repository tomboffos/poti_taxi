<?php

namespace App\Http\Resources\User\Restaurants;

use Illuminate\Http\Resources\Json\JsonResource;

class DishCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'dishes' => $this->dishes
        ];
    }
}
