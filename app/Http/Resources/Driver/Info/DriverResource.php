<?php

namespace App\Http\Resources\Driver\Info;

use App\Services\Driver\Auth\ImageService;
use Illuminate\Http\Resources\Json\JsonResource;

class DriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'iin' => $this->iin,
            'car_model' => $this->car_model,
            'car_id' => $this->car_id,
            'car_color' => $this->car_color,
            'driver_image' => ImageService::showImages($this->driver_image),
            'avatar' => asset($this->avatar),
            'accepted' => $this->accepted,
            'blocked' => $this->blocked
        ];
    }
}
