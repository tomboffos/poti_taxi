<?php

namespace App\Http\Resources\Driver\Info;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'surname' => $this->surname,
            'phone' => $this->phone,
            'driver' => new DriverResource($this->driver),
            'role_id' => $this->role_id
        ];
    }
}
