<?php


namespace App\Http\Services;


use App\Models\Driver;
use App\Models\DriverSubscription;
use Carbon\Carbon;

class DriverSubscriptionService
{
    public function checkDrivers()
    {
        foreach (Driver::get() as $driver)
            $this->checkSubscription($driver);
    }

    public function checkSubscription(Driver $driver)
    {
        $subscription = $driver->subscriptions()->orderBy('id', 'desc')->first();
        if ($subscription) {
            if ($subscription->subscription->duration < Carbon::now()->diffInDays(Carbon::parse($subscription->payed_at)))
                $driver->update([
                    'blocked' => true
                ]);
        }
    }
}
