<?php

namespace App\Http\Requests\Api\Driver\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'iin' => 'required|min:11|unique:drivers,iin',
            'surname' => 'required',
            'name' => 'required',
            'car_model' => 'required',
            'car_color' => 'required',
            'car_id' => 'required|min:7|unique:drivers,car_id',
            'driver_image' => 'required',
            'password' => 'required|min:8',
            'avatar' => 'required',
            'phone' => 'required|unique:users,phone',
            'token' => 'required',
        ];
    }
}
