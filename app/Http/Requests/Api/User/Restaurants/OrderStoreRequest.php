<?php

namespace App\Http\Requests\Api\User\Restaurants;

use Illuminate\Foundation\Http\FormRequest;

class OrderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'point' => 'required',
            'basket' => 'required',
            'organization_id' => 'required|exists:organizations,id'
        ];
    }
}
