<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'area_id',
        'driver_id',
        'point',
        'cancel_reason_id',
        'cancel_comment',
        'user_id',
        'order_status_id',
        'start_point',
        'organization_id',
        'order_type_id',
        'price'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function cancelReason()
    {
        return $this->belongsTo(CancelReason::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }
}

