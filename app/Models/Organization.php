<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'image',
        'description',
        'short_description',
        'point',
        'area_id'
    ];

    public function categories()
    {
        return $this->hasMany(DishCategory::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }
}
