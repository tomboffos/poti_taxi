<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use HasFactory, SoftDeletes;

    public function dish()
    {
        return $this->belongsTo(Dish::class);
    }

    protected $fillable = [
        'order_id',
        'dish_id',
        'quantity',
        'price'
    ];
}
