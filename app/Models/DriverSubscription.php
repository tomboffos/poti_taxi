<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DriverSubscription extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'subscription_id',
        'payment_status_id',
        'driver_id',
        'payed'
    ];


    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }
}
