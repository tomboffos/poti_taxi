<?php

namespace App\Models;

use App\Services\Driver\Auth\ImageService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Driver extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    protected $fillable = [
        'iin',
        'car_model',
        'car_color',
        'car_id',
        'avatar',
        'driver_image',
        'user_id',
        'accepted',
        'blocked'
    ];
    use HasFactory, SoftDeletes;

    protected $casts = [
        'driver_image' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function setAvatarAttribute($value)
    {
        $this->attributes['avatar'] = !is_string($value) ? $value->store('avatars') : $this->attributes['avatar'];
    }

    public function subscriptions()
    {
        return $this->hasMany(DriverSubscription::class);
    }

    public function setDriverImageAttribute($value)
    {
        $this->attributes['driver_image'] = ImageService::storeImages($value);
    }

}
