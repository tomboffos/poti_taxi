<?php

namespace App\Models;

use App\Services\Driver\Map\MapService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDriver extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'sent',
        'location',
        'user_id',
        'order_id',
        'distance'
    ];

    public function driver()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }


}
