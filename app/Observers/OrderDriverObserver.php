<?php

namespace App\Observers;

use App\Http\Resources\User\Interactions\OrderResource;
use App\Models\OrderDriver;
use App\Services\Main\PushNotification;
use App\Services\Main\WebSocket;

class OrderDriverObserver
{
    /**
     * Handle the OrderDriver "created" event.
     *
     * @param  \App\Models\OrderDriver  $orderDriver
     * @return void
     */
    public function created(OrderDriver $orderDriver)
    {
        //
    }

    /**
     * Handle the OrderDriver "updated" event.
     *
     * @param  \App\Models\OrderDriver  $orderDriver
     * @return void
     */
    public function updated(OrderDriver $orderDriver)
    {
        //
        if ($orderDriver->getOriginal('sent') != $orderDriver->sent){
//            g
            WebSocket::sendEvent('order-sent-request-'.$orderDriver->user_id,new OrderResource($orderDriver->order));
        }
    }

    /**
     * Handle the OrderDriver "deleted" event.
     *
     * @param  \App\Models\OrderDriver  $orderDriver
     * @return void
     */
    public function deleted(OrderDriver $orderDriver)
    {
        //
    }

    /**
     * Handle the OrderDriver "restored" event.
     *
     * @param  \App\Models\OrderDriver  $orderDriver
     * @return void
     */
    public function restored(OrderDriver $orderDriver)
    {
        //
    }

    /**
     * Handle the OrderDriver "force deleted" event.
     *
     * @param  \App\Models\OrderDriver  $orderDriver
     * @return void
     */
    public function forceDeleted(OrderDriver $orderDriver)
    {
        //
    }
}
