<?php

namespace App\Observers;

use App\Http\Resources\Api\User\Restaurants\OrderRestaurantResource;
use App\Http\Resources\User\Interactions\OrderResource;
use App\Models\Order;
use App\Services\Main\PushNotification;
use App\Services\Main\WebSocket;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function created(Order $order)
    {
        //
        if ($order->order_type_id == 2)
            WebSocket::sendEvent("order-created", new OrderRestaurantResource($order));
        else
            WebSocket::sendEvent("order-created", new OrderResource($order));

    }

    /**
     * Handle the Order "updated" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function updated(Order $order)
    {
        //
        if ($order->getOriginal('driver_id') == null && $order->getOriginal('driver_id') != $order->driver_id) {
            PushNotification::send($order->user->devices(), 'Уведомление по заказу ' . $order->id,
                "Водитель " . $order->driver->car_model . " по гос.номеру " . $order->driver->car_id . " принял заказ ");
            if ($order->order_type_id == 2)
                WebSocket::sendEvent("order-accepted-$order->id", new OrderRestaurantResource($order));
            else
                WebSocket::sendEvent("order-accepted-$order->id", new OrderResource($order));
        }


        if ($order->getOriginal('order_status_id') != $order->order_status_id)
            switch ($order->order_status_id) {
                case 3:
                    PushNotification::send($order->user->devices(), 'Уведомление по заказу ' . $order->id,
                        'К сожалению водитель отменил заказ');
                    WebSocket::sendEvent("order-cancelled-by-driver-$order->id", new OrderResource($order));

                    break;
                case 4:
                    PushNotification::send($order->driver->user->devices(), 'Уведомление по заказу ' . $order->id,
                        'К сожалению клиент отменил заказ');
                    WebSocket::sendEvent("order-cancelled-by-client-$order->id", new OrderResource($order));
                    break;
                case 5:
                    WebSocket::sendEvent("order-done-$order->id", new OrderResource($order));
                    break;
            }
    }

    /**
     * Handle the Order "deleted" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the Order "restored" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the Order "force deleted" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
