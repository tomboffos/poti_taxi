<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('area') }}'><i class='nav-icon la la-question'></i> Площади</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('user') }}'><i class='nav-icon la la-question'></i> Пользователи</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('driver') }}'><i class='nav-icon la la-question'></i> Водители</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i class="nav-icon la la-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('subscription') }}'><i class='nav-icon la la-question'></i> Тарифы</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('cancel-reason') }}'><i class='nav-icon la la-question'></i> Причины отказов</a></li>
