function initMap(currentPosition){
    console.log(currentPosition)
    const uluru = { lat: currentPosition.coords.latitude, lng: currentPosition.coords.longitude };
    // The map, centered at Uluru
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 15,
        center: uluru,
    });


    map.addListener('click',(e)=>{
        addPolygons(e.latLng,map)

    })
}
let polylines = []
let polygon = new google.maps.Polygon({
    paths: polylines,
    strokeColor: "#FF0000",
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: "#FF0000",
    fillOpacity: 0.35,
    map
})

function addPolygons(latLng,map){
    polylines.push(latLng.toJSON())
    polygon.setMap(null)
    polygon = new google.maps.Polygon({
        paths: polylines,
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        map
    })
    setField()
}

function setField() {
    let areaField = document.querySelectorAll('input[name="area"]')
    areaField[0].value = JSON.stringify(polylines.map((latLng)=>[
        latLng.lat,
        latLng.lng
    ]))
}

if(navigator.geolocation){
    navigator.geolocation.getCurrentPosition(initMap);
}
