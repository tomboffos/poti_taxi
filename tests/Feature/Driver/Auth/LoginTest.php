<?php

namespace Tests\Feature\Driver\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->postJson('/api/auth/driver/login',[
            'phone' => '+7 (707) 302-88-07',
            'password' => '11072000A'
        ]);

        $response->dump();

        $response->assertStatus(200);
    }
}
