<?php

namespace Tests\Feature\Driver\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        Storage::fake('driver_profiles');
        Storage::fake('avatars');

        $driverProfile[] = UploadedFile::fake()->image('avatar.jpg');
        $driverProfile[] = UploadedFile::fake()->image('avatar2.jpg');
        $fakeAvatar = UploadedFile::fake()->image('avatar_main.jpg');
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->postJson('/api/auth/driver/register', [
            'phone' => '+7 (707) 302-88-01',
            'iin' => '000711500852',
            'name' => 'Asyl taxist',
            'car_model' => 'Mercedes',
            'car_color' => 'red',
            'car_id' => '007KAX02',
            'driver_image' => $driverProfile,
            'avatar' => $fakeAvatar,
            'password' => '11072000A',
            'surname' => 'Tasbulatov'
        ]);
//        Storage::disk('driver_profiles')->assertExists($driverProfile[0]->hashName());
//        Storage::disk('avatars')->assertExists($fakeAvatar->hashName());

        $response->dump();
        $response->assertStatus(201);
    }
}
