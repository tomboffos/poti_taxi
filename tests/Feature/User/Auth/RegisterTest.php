<?php

namespace Tests\Feature\User\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // 1|cdNDZJVo9Zw0jtzjW3soK2bBJB77a6AZqkY8PgtS
    public function test_example()
    {
        $response = $this->postJson('/api/auth/user/register',[
            'phone' => '+7 (707) 303-99-17',
            'password' => '11072000A',
            'name' => 'Асыл'
        ]);

        $response->dump();

        $response->assertStatus(422);
    }
}
