<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FetchOrganizationsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer 2|c0eUu44ICdQYObl4X2u6t1qICG3ufA0O7zNMDxwr'
        ])->get('/api/user/organizations?point=[43.20741440266386, 76.8814692539062]');

        $response->assertStatus(200);
    }
}
