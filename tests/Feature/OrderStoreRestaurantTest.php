<?php

namespace Tests\Feature;

use App\Models\Dish;
use App\Models\Organization;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderStoreRestaurantTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {

        $response = $this->withHeaders([
            'Authorization' => 'Bearer 2|c0eUu44ICdQYObl4X2u6t1qICG3ufA0O7zNMDxwr'
        ])->postJson('/api/user/restaurants/orders', [
            'point' => json_encode([43.20741440266386, 76.8814692539062]),
            'organization_id' => Organization::inRandomOrder()->first()->id,
            'basket' => json_encode([
                [
                    'id' => Dish::inRandomOrder()->first()->id,
                    'quantity' => 500,
                ]
            ])
        ]);

        $response->dump();
        $response->assertStatus(201);
    }
}
