<?php

use App\Http\Controllers\Api\Driver\Auth\LoginController;
use App\Http\Controllers\Api\Driver\Auth\RegisterController;
use App\Http\Controllers\Api\Driver\Interactions\CancelReasonController;
use App\Http\Controllers\Api\Driver\Interactions\DriverOrderController;
use App\Http\Controllers\Api\Driver\Interactions\DriverSubscriptionController;
use App\Http\Controllers\Api\Driver\Interactions\OrderDriverController;
use App\Http\Controllers\Api\Driver\Interactions\SubscriptionController;
use App\Http\Controllers\Api\Driver\Main\DriverController;
use App\Http\Controllers\Api\User\Interactions\OrderController;
use App\Http\Controllers\Api\User\Main\UserController;
use App\Http\Controllers\Api\User\Restaurants\OrderRestaurantController;
use App\Http\Controllers\Api\User\Restaurants\OrganizationCategoryController;
use App\Http\Controllers\Api\User\Restaurants\OrganizationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::get('/areas', function (Request $request) {
//    $service = new \App\Services\User\Interactions\OrderService();
//    return $service->findInArray($request->point);
//
//});

Route::post('/subscription', [DriverSubscriptionController::class, 'result'])->name('subscription.result');

Route::group(['prefix' => 'auth'], function () {
    Route::group(['prefix' => 'driver'], function () {
        Route::post('/login', [LoginController::class, 'login']);
        Route::post('/register', [RegisterController::class, 'register']);
    });
    Route::post('/forget', [\App\Http\Controllers\Api\User\Auth\LoginController::class, 'forget']);
    Route::group(['prefix' => 'user'], function () {
        Route::post('/login', [\App\Http\Controllers\Api\User\Auth\LoginController::class, 'login']);
        Route::post('/register', [\App\Http\Controllers\Api\User\Auth\RegisterController::class, 'register']);
    });
});
Route::resource('cancel-reason', CancelReasonController::class);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::post('/logout', [UserController::class, 'logout']);
        Route::resources([
            'orders' => OrderController::class,
            '/' => UserController::class,
            'organizations' => OrganizationController::class,
            'organization-category' => OrganizationCategoryController::class
        ]);
        Route::group(['prefix' => 'restaurants'], function () {
            Route::resources([
                'orders' => OrderRestaurantController::class
            ]);
        });
    });
    Route::group(['prefix' => 'driver'], function () {
        Route::resources([
            'orders' => DriverOrderController::class,
            'subscription' => SubscriptionController::class,
            '/' => DriverController::class,
            'driver-subscription' => DriverSubscriptionController::class,
            'order-driver' => OrderDriverController::class,

        ]);
    });
});
