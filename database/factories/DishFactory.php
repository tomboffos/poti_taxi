<?php

namespace Database\Factories;

use App\Models\Dish;
use App\Models\DishCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class DishFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Dish::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->name,
            'image' => $this->faker->imageUrl,
            'description' => $this->faker->text,
            'price' => mt_rand(1000, 50000),
            'dish_category_id' => DishCategory::inRandomOrder()->first()->id

        ];
    }
}
