<?php

namespace Database\Factories;

use App\Models\Area;
use App\Models\Organization;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrganizationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Organization::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->word,
            'image' => $this->faker->imageUrl,
            'description' => $this->faker->text,
            'short_description' => $this->faker->title,
            'area_id' => Area::inRandomOrder()->first()->id,

        ];
    }
}
