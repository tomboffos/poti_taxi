<?php

namespace Database\Seeders;

use App\Models\Dish;
use App\Models\DishCategory;
use App\Models\Organization;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Organization::factory(50)->create();
        DishCategory::factory(150)->create();
        Dish::factory(1000)->create();
        $this->call([
//            OrderStatusSeeder::class,
//            PaymentStatusSeeder::class
//            RoleSeeder::class
        ]);
    }
}
