<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('order_statuses')->insert([
            'name' => 'Новый заказ',
            'id' => 1
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Водитель принял заказ',
            'id' => 2
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Заказ отменен водителем',
            'id' => 3
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Заказ отменен пассажиром',
            'id' => 4
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Заказ был сделан',
            'id' => 5
        ]);
    }
}
