<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderStatusRestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('order_statuses')->insert([
           'id' => 6,
           'name' => 'Новый заказ доставки еды'
        ]);


        DB::table('order_statuses')->insert([
            'id' => 7,
            'name' => 'Ресторан принял ваш заказ'
        ]);


        DB::table('order_statuses')->insert([
            'id' => 8,
            'name' => 'Ресторан отклонил ваш заказ'
        ]);


        DB::table('order_statuses')->insert([
            'id' => 9,
            'name' => 'Курьер получил заказ'
        ]);

        DB::table('order_statuses')->insert([
            'id' => 10,
            'name' => 'Курьер доставил заказ'
        ]);
    }
}
